﻿using Mammoth.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mammoth.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BreakFastPage : ContentPage
	{
		public BreakFastPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);

		}

		private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{

			var x = e.SelectedItem as ItemsModel;
			if(x==null)
			{ return; }
			else
			{
				Navigation.PushAsync(new BreakFastOrderPage());
				ItemList.SelectedItem = null;
			}
			
			
		}
	}
}