﻿using Mammoth.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Mammoth.Services
{
	public class Test
	{ 
        
        
        public Test()
        {

        }

        public ObservableCollection<ItemsModel> BreakFastItems = new ObservableCollection<ItemsModel>()
             {  new ItemsModel()
                         {
                            ImageSource="Eggs.jpg", ItemName="Eggs", Price=2
                       },
                      
              new ItemsModel()
                    {
                        ImageSource = "PanCake.jpg", ItemName = "PanCakes", Price=3
                    },
             
         new ItemsModel()
        {
            ImageSource = "Bacon.jpg", ItemName = "Bacons",Price=4
                    }
    };
        public ObservableCollection<ItemsModel> LunchItems = new ObservableCollection<ItemsModel>()
             {  new ItemsModel()
                         {
                            ImageSource="Burger.jpg", ItemName="Burger", Price=2
                       },

              new ItemsModel()
                    {
                        ImageSource = "HotDog.jpg", ItemName = "HOTDOG", Price=3
                    },

         new ItemsModel()
        {
            ImageSource = "Sandwich.jpg", ItemName = "Sandwich",Price=4
                    }
    };
        public ObservableCollection<ItemsModel> DinnerItems = new ObservableCollection<ItemsModel>()
             {  new ItemsModel()
                         {
                            ImageSource="Soup.jpg", ItemName="Soup", Price=2
                       },

              new ItemsModel()
                    {
                        ImageSource = "Pasta.jpg", ItemName = "Pasta", Price=3
                    }
    };
        public ObservableCollection<ItemsModel> DessertItems = new ObservableCollection<ItemsModel>()
             {  new ItemsModel()
                         {
                            ImageSource="Cake.jpg", ItemName="Cake", Price=2
                       },

              new ItemsModel()
                    {
                        ImageSource = "Jelly.jpg", ItemName = "Jelly", Price=3
                    }
    };

    }
}
