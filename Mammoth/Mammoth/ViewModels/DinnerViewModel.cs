﻿using Mammoth.Models;
using Mammoth.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace Mammoth.ViewModels
{
    public	class DinnerViewModel: INotifyPropertyChanged
	{
		public DinnerViewModel()
		{
			Test test = new Test();
			DinnerItem = test.DinnerItems;
		}
		private ObservableCollection<ItemsModel> _break;
		public ObservableCollection<ItemsModel> DinnerItem { get { return _break; } set { _break = value; OnPropertyChanged(); } }
		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged(
	[CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this,
			new PropertyChangedEventArgs(propertyName));
		}
	}
}
